package epam.semenkovich.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import epam.semenkovich.entity.Publisher;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;

public class PublisherDAO extends AbstractDAO<String, Publisher> {
	private static Logger log = Logger.getLogger(PublisherDAO.class);
	
	public static final String SQL_SELECT_ALL_PUBLISHERS = "SELECT * FROM publisher";
	public static final String SQL_INSERT_PUBLISHER = "INSERT INTO publisher(name,address) VALUES(?,?)";
	public static final String SQL_SELECT_PUBLISHER_NAME_BY_ID = "SELECT * FROM publisher WHERE idPublisher=?";

	@Override
	public List<Publisher> findAll() {
		List<Publisher> publishers = new ArrayList<Publisher>();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ALL_PUBLISHERS);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				Publisher publisher = new Publisher();
				publisher.setIdPublisher(resultSet.getInt("idPublisher"));
				publisher.setName(resultSet.getString("name"));
				publisher.setAddress(resultSet.getString("address"));
				publishers.add(publisher);
			}
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return publishers;
	}

	@Override
	public Publisher findEntityById(String id) {
		Publisher publisher = null;
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_PUBLISHER_NAME_BY_ID);
			st.setString(1, id);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				publisher = new Publisher();
				publisher.setIdPublisher(resultSet.getInt("idPublisher"));
				publisher.setName(resultSet.getString("name"));
				publisher.setAddress(resultSet.getString("address"));
			}
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return publisher;
	}

	@Override
	public boolean delete(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Publisher entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Publisher entity) {
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_INSERT_PUBLISHER);
			st.setString(1, entity.getName());
			st.setString(2, entity.getAddress());
			st.executeUpdate();
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
			return false;
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return true;
	}

	@Override
	public Publisher update(Publisher entity) {
		// TODO Auto-generated method stub
		return null;
	}
}
