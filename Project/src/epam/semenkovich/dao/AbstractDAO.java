package epam.semenkovich.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import epam.semenkovich.entity.Entity;

public abstract class AbstractDAO<K, T extends Entity> {
	public abstract List<T> findAll();

	public abstract T findEntityById(K id);

	public abstract boolean delete(K id);

	public abstract boolean delete(T entity);

	public abstract boolean create(T entity);

	public abstract T update(T entity);

	public void close(Statement st) {
		try {
			if (st != null) {
			st.close();
			}
		} catch (SQLException e) {
			// ��� � ������������� �������� Statement
		}
	}


}
