package epam.semenkovich.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import epam.semenkovich.entity.User;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;
import epam.semenkovich.security.Hash;

public class UserDAO extends AbstractDAO<String, User> {
	private static Logger log = Logger.getLogger(UserDAO.class);
	
	public static final String SQL_SELECT_ALL_USERS = "SELECT * FROM user";
	public static final String SQL_SELECT_USER_BY_LOGIN_AND_PASS = "SELECT role,login,password FROM user WHERE login=? AND password=?";
	public static final String SQL_INSERT_USER = "INSERT INTO user(role,login,password) VALUES(?,?,?)";

	/**
	 * finds every user in database
	 * 
	 * @return List of all users in database
	 */
	@Override
	public List<User> findAll() {
		List<User> users = new ArrayList<User>();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ALL_USERS);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setRole(resultSet.getString("role"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
				users.add(user);
			}
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return users;
	}

	/**
	 * finds user by his login and password
	 * 
	 * @param login
	 *            Users login in String form
	 * @param pass
	 *            Users password in String form
	 * @return Users that has login and pass as sets in parameters
	 */
	public User findUserByLoginAndPass(String login, String pass) {
		User user = null;
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_USER_BY_LOGIN_AND_PASS);
			st.setString(1, login);
			st.setString(2, Hash.toHash(pass));
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				user = new User();
				user.setRole(resultSet.getString("role"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return user;
	}

	@Override
	public boolean create(User user) {
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_INSERT_USER);
			st.setString(1, "USER");
			st.setString(2, user.getLogin());
			st.setString(3, Hash.toHash(user.getPassword()));
			st.executeUpdate();
		} catch (SQLException | ResourceException e) {
			log.error("SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
			return false;
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return true;
	}

	@Override
	public User update(User user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public User findEntityById(String login) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean delete(String id) {
		throw new UnsupportedOperationException();

	}

	@Override
	public boolean delete(User entity) {
		throw new UnsupportedOperationException();
	}

}
