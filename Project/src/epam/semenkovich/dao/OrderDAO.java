package epam.semenkovich.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import epam.semenkovich.entity.Order;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;

public class OrderDAO extends AbstractDAO<String, Order> {
	private static Logger log = Logger.getLogger(OrderDAO.class);

	public static final String SQL_SELECT_ALL_ORDERS_BY_USER = "SELECT order.idOrder,order.user,order.idIssue,order.isPaid,order.Cost,issue.name FROM `order` JOIN issue ON issue.idIssue=order.idIssue WHERE `user` = ? AND order.isDeleted=0";
	public static final String SQL_INSERT_ORDER = "INSERT INTO `order`(user,idIssue,cost) VALUES(?,?,?)";
	public static final String SQL_DELETE_ORDER_BY_ID = "UPDATE `order` SET isDeleted=1 WHERE idOrder=?";
	public static final String SQL_UPDATE_ORDER_BY_ID = "UPDATE `order` SET isPaid=1 WHERE idOrder=?";

	@Override
	public List<Order> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Order> findOrdersByUser(String user) {
		List<Order> orders = new ArrayList<Order>();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ALL_ORDERS_BY_USER);
			st.setString(1, user);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				Order order = new Order();
				order.setIdOrder(resultSet.getInt("idOrder"));
				order.setIdIssue(resultSet.getInt("idIssue"));
				order.setUser(resultSet.getString("user"));
				order.setTotalCost(resultSet.getInt("cost"));
				order.setIsPaid(resultSet.getInt("isPaid"));
				Map<Integer, String> map = new HashMap<Integer, String>();
				map.put(resultSet.getInt("idIssue"),
						resultSet.getString("name"));
				order.setId_name_issue(map);
				orders.add(order);
			}
		} catch (SQLException | ResourceException e) {
			log.error("SelectAll: SQL exception (request or table failed) or there is no free connecthion "
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return orders;
	}

	@Override
	public Order findEntityById(String user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(String id) {
		boolean flag = false;
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_DELETE_ORDER_BY_ID);
			st.setInt(1, Integer.parseInt(id));
			st.executeUpdate();
			flag = true;
		} catch (SQLException | ResourceException e) {
			log.error("Delete: SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return flag;
	}

	public boolean updateIsPaid(String id) {
		boolean flag = false;
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_UPDATE_ORDER_BY_ID);
			st.setInt(1, Integer.parseInt(id));
			st.executeUpdate();
			flag = true;
		} catch (SQLException | ResourceException e) {
			log.error("Update: SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return flag;
	}

	@Override
	public boolean delete(Order entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Order entity) {
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_INSERT_ORDER);
			st.setString(1, entity.getUser());
			st.setInt(2, entity.getIdIssue());
			st.setInt(3, entity.getTotalCost());
			st.executeUpdate();
		} catch (SQLException | ResourceException e) {
			log.error("Insert: SQL exception (request or table failed) or there is no free connecthion"
					+ e.getMessage());
			return false;
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return true;
	}

	@Override
	public Order update(Order entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
