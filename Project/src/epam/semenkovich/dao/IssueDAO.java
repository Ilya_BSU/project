package epam.semenkovich.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;

public class IssueDAO extends AbstractDAO<String, Issue> {
	private static Logger log = Logger.getLogger(IssueDAO.class);

	public static final String SQL_SELECT_ALL_ISSUES = "SELECT issue.idIssue,issue.name,issue.idCathegory,issue.pages,issue.monthCost,issue.idPublisher,publisher.name as publisherName,cathegory.name as cathegoryName FROM issue JOIN publisher ON publisher.idPublisher=issue.idPublisher JOIN cathegory ON cathegory.idCathegory=issue.idCathegory WHERE isDeleted=0;";
	public static final String SQL_INSERT_ISSUE = "INSERT INTO issue(name,idCathegory,pages,monthCost,idPublisher) VALUES(?,?,?,?,?)";
	public static final String SQL_DELETE_ISSUE_BY_ID = "UPDATE issue SET isDeleted=1 WHERE idIssue=?";
	public static final String SQL_SELECT_ISSUE_BY_ID = "SELECT * FROM `issue` WHERE `idIssue`=?";
	public static final String SQL_UPDATE_ISSUE_BY_ID = "UPDATE issue SET name=?,idCathegory=?,pages=?,monthCost=?,idPublisher=? WHERE idIssue=?";

	@Override
	public List<Issue> findAll() {
		List<Issue> issues = new ArrayList<Issue>();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ALL_ISSUES);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				Issue issue = new Issue();
				issue.setId(resultSet.getInt("idIssue"));
				issue.setName(resultSet.getString("name"));
				issue.setIdCathegory(resultSet.getInt("idCathegory"));
				issue.setPages(resultSet.getInt("pages"));
				issue.setMonthCost(resultSet.getInt("monthCost"));
				issue.setIdPublisher(resultSet.getInt("idPublisher"));
				Map<Integer, String> mapPublisher = new HashMap<Integer, String>();
				log.debug(resultSet.getInt("idPublisher"));
				log.debug(resultSet.getString("publisherName"));
				mapPublisher.put(resultSet.getInt("idPublisher"),
						resultSet.getString("publisherName"));
				issue.setId_name_publisher(mapPublisher);
				Map<Integer, String> mapCathegory = new HashMap<Integer, String>();
				mapCathegory.put(resultSet.getInt("idCathegory"),
						resultSet.getString("cathegoryName"));
				issue.setId_name_cathegory(mapCathegory);
				issues.add(issue);
			}
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return issues;
	}

	@Override
	public Issue findEntityById(String id) {
		Issue issue = new Issue();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ISSUE_BY_ID);
			st.setInt(1, Integer.parseInt(id));
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				issue.setId(resultSet.getInt("idIssue"));
				issue.setName(resultSet.getString("name"));
				issue.setIdCathegory(resultSet.getInt("idCathegory"));
				issue.setPages(resultSet.getInt("pages"));
				issue.setMonthCost(resultSet.getInt("monthCost"));
				issue.setIdPublisher(resultSet.getInt("idPublisher"));
			}
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return issue;
	}

	@Override
	public boolean delete(String id) {
		boolean flag = false;
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_DELETE_ISSUE_BY_ID);
			st.setInt(1, Integer.parseInt(id));
			st.executeUpdate();
			flag = true;
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return flag;
	}

	@Override
	public boolean delete(Issue entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Issue entity) {
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_INSERT_ISSUE);
			st.setString(1, entity.getName());
			st.setInt(2, entity.getIdCathegory());
			st.setInt(3, entity.getPages());
			st.setInt(4, entity.getMonthCost());
			st.setInt(5, entity.getIdPublisher());
			st.executeUpdate();
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
			return false;
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return true;
	}

	@Override
	public Issue update(Issue entity) {
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_UPDATE_ISSUE_BY_ID);
			st.setString(1, entity.getName());
			st.setInt(2, entity.getIdCathegory());
			st.setInt(3, entity.getPages());
			st.setInt(4, entity.getMonthCost());
			st.setInt(5, entity.getIdPublisher());
			st.setInt(6, entity.getId());
			st.executeUpdate();
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
			return null;
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return entity;
	}

}
