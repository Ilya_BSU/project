package epam.semenkovich.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import epam.semenkovich.entity.Cathegory;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;

public class CathegoryDAO extends AbstractDAO<String, Cathegory> {
	private static Logger log = Logger.getLogger(CathegoryDAO.class);

	public static final String SQL_SELECT_ALL_CATHEGORYS = "SELECT * FROM cathegory";
	public static final String SQL_SELECT_CATHEGORY_BY_ID = "SELECT * FROM cathegory WHERE idCathegory=?";

	@Override
	public List<Cathegory> findAll() {
		List<Cathegory> cathegorys = new ArrayList<Cathegory>();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_ALL_CATHEGORYS);
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				Cathegory cathegory = new Cathegory();
				cathegory.setIdCathegory(resultSet.getInt("idCathegory"));
				cathegory.setName(resultSet.getString("name"));
				cathegorys.add(cathegory);
			}
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return cathegorys;
	}

	@Override
	public Cathegory findEntityById(String id) {
		Cathegory cathegory = new Cathegory();
		Connection cn = null;
		PreparedStatement st = null;
		try {
			cn = ConnectionPool.getInstance().getResource();
			st = cn.prepareStatement(SQL_SELECT_CATHEGORY_BY_ID);
			st.setInt(1, Integer.parseInt(id));
			ResultSet resultSet = st.executeQuery();
			while (resultSet.next()) {
				cathegory.setIdCathegory(resultSet.getInt("idCathegory"));
				cathegory.setName(resultSet.getString("name"));
			}
		} catch (SQLException | ResourceException e) {
			log.error("Error while forming resultSet or there is no free connecthion"
					+ e.getMessage());
		} finally {
			close(st);
			if (cn != null) {
				ConnectionPool.getInstance().returnResource(cn);
			}
		}
		return cathegory;
	}

	@Override
	public boolean delete(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Cathegory entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Cathegory entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cathegory update(Cathegory entity) {
		// TODO Auto-generated method stub
		return null;
	}
}
