package epam.semenkovich.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import epam.semenkovich.exception.ResourceException;

public class ConnectionPool {
	private static Logger log = Logger.getLogger(ConnectionPool.class);
	public final static int POOL_SIZE = 10;
	private static final int DELAY = 5000;
	private BlockingQueue<Connection> resources = new ArrayBlockingQueue<Connection>(
			POOL_SIZE);
	private static ConnectionPool pool;
	private static AtomicBoolean isCreated = new AtomicBoolean(false);
	private static Lock lock = new ReentrantLock();

	public static ConnectionPool getInstance() {

		if (!isCreated.get()) {
			lock.lock();
			if (pool == null) {
				pool = new ConnectionPool();
				isCreated.set(true);
			}
			lock.unlock();
		}
		return pool;
	}

	private ConnectionPool() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());

			ResourceBundle resource = ResourceBundle
					.getBundle("resources/database");
			String url = resource.getString("db.url");
			String user = resource.getString("db.user");
			String pass = resource.getString("db.password");
			for (int i = 0; i < POOL_SIZE; i++) {
				resources.add(DriverManager.getConnection(url, user, pass));
			}
		} catch (SQLException e) {
			log.fatal("connecthion pool inithializathion failed");
			throw new RuntimeException(" Error while creating pool!\n"
					+ e.getMessage());
		}
	}

	public Connection getResource() throws ResourceException {

		if (isCreated.get()) {
			return resources.poll();
		} else {
			throw new ResourceException("Cant get connection");
		}

	}

	public void returnResource(Connection res) {
		resources.add(res);
	}

	public void closePool() {
		isCreated.set(false);
		try {
			TimeUnit.MILLISECONDS.sleep(DELAY);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
		try {
			for (int i = 0; i < POOL_SIZE; i++) {
				resources.poll().close();
			}
		} catch (SQLException e) {
			log.error("Error while closing pool's connection" + e.getMessage());
		}
	}
}
