package epam.semenkovich.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import epam.semenkovich.command.ActionCommand;
import epam.semenkovich.factory.ActionFactory;
import epam.semenkovich.pool.ConnectionPool;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/controller" }, initParams = { @WebInitParam(name = "logFileName", value = "/WEB-INF/classes/resources/log4j.properties") })
public class Controller extends HttpServlet {
	private static Logger log = Logger.getLogger(Controller.class);

	@Override
	public void init() throws ServletException {
		ServletConfig sc = this.getServletConfig();
		Enumeration e = sc.getInitParameterNames();
		String name = (String) e.nextElement();
		String logFileName = sc.getInitParameter(name);
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(getServletContext()
					.getRealPath(logFileName)));
			PropertyConfigurator.configure(properties);
			log.debug("init");
		} catch (IOException ex) {
			throw new ExceptionInInitializerError("Log Initialization Error: "
					+ ex.getMessage());
		}
	}

	@Override
	public void destroy() {
		ConnectionPool.getInstance().closePool();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String page = null;
		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);
		if (command != null) {
			page = command.execute(request);
		}
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher(page);

			dispatcher.forward(request, response);
		} else {
			page = ConfigurationManager.getProperty("path.page.index");
			request.getSession().setAttribute("nullPage",
					new MessageManager().getProperty("message.nullpage"));
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}