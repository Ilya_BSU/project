package epam.semenkovich.logic;

import java.util.List;

import epam.semenkovich.dao.PublisherDAO;
import epam.semenkovich.entity.Publisher;

public class PublisherLogic {
	public static List<Publisher> recievePublishers() {
		PublisherDAO dao = new PublisherDAO();
		return dao.findAll();
	}

	public static Publisher getPublisherById(String id) {
		if (id.isEmpty()) {
			return null;
		}
		PublisherDAO dao = new PublisherDAO();
		return dao.findEntityById(id);
	}

	public static Publisher addPublisher(String name, String address) {
		if (name.isEmpty() || address.isEmpty()) {
			return null;
		}
		PublisherDAO dao = new PublisherDAO();
		Publisher publisher = new Publisher();
		publisher.setName(name);
		publisher.setAddress(address);
		return dao.create(publisher) ? publisher : null;
	}

}
