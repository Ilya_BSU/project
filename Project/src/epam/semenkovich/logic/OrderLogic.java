package epam.semenkovich.logic;

import java.util.List;

import org.apache.log4j.Logger;

import epam.semenkovich.dao.OrderDAO;
import epam.semenkovich.entity.Order;

public class OrderLogic {
	private static Logger log = Logger.getLogger(OrderLogic.class);
	public static boolean addOrder(String user, String idIssue, String cost) {
		if (orderValidation(user,idIssue,cost)) {
			return false;
		}
		OrderDAO dao = new OrderDAO();
		Order order = new Order();
		order.setUser(user);
		order.setIdIssue(Integer.parseInt(idIssue));
		order.setTotalCost(Integer.parseInt(cost));
		return dao.create(order);
	}

	public static boolean deleteOrderById(String idOrder) {
		if (idOrder.isEmpty() || Integer.parseInt(idOrder) < 1) {
			return false;
		}
		OrderDAO dao = new OrderDAO();
		return dao.delete(idOrder);
	}

	public static boolean PayOrder(String idOrder) {
		if (idOrder.isEmpty() || Integer.parseInt(idOrder) < 1) {
			return false;
		}
		OrderDAO dao = new OrderDAO();
		return dao.updateIsPaid(idOrder);
	}

	public static List<Order> getOrdersByUser(String user) {
		if (user.isEmpty()) {
			return null;
		}
		OrderDAO dao = new OrderDAO();
		return dao.findOrdersByUser(user);
	}

	private static boolean orderValidation(String user, String idIssue,
			String cost) {
		boolean flag = false;
		try {
			flag = user.isEmpty() || idIssue.isEmpty() || cost.isEmpty()
					|| Integer.parseInt(cost) < 0;
		} catch (NumberFormatException e) {
			flag = true;
			log.error("Comes Order's string cost field without number");
		}
		return flag;
	}
}
