package epam.semenkovich.logic;

import java.util.List;

import org.apache.log4j.Logger;

import epam.semenkovich.dao.IssueDAO;
import epam.semenkovich.entity.Issue;

public class IssueLogic {
	private static Logger log = Logger.getLogger(IssueLogic.class);
	public static List<Issue> recieveIssues() {
		IssueDAO dao = new IssueDAO();
		return dao.findAll();
	}

	public static boolean deleteIssueById(String id) {
		if (id.isEmpty() || Integer.parseInt(id) < 1) {
			return false;
		}
		IssueDAO dao = new IssueDAO();
		return dao.delete(id) ? true : false;
	}

	public static Issue getIssueById(String id) {
		if (id.isEmpty() || Integer.parseInt(id) < 1) {
			return null;
		}
		IssueDAO dao = new IssueDAO();
		return dao.findEntityById(id);
	}

	public static Issue addIssue(String name, String cathegory, String pages,
			String cost, String publisher) {
		if (issueValidation(name, cathegory, pages, cost, publisher)) {
			return null;
		}
		IssueDAO dao = new IssueDAO();
		Issue issue = new Issue();
		issue.setName(name);
		issue.setIdCathegory(Integer.parseInt(cathegory));
		issue.setIdPublisher(Integer.parseInt(publisher));
		issue.setMonthCost(Integer.parseInt(cost));
		issue.setPages(Integer.parseInt(pages));
		return dao.create(issue) ? issue : null;

	}

	private static boolean issueValidation(String name, String cathegory,
			String pages, String cost, String publisher) {
		boolean flag = false;
		try {
			flag = name.isEmpty() || cathegory.isEmpty() || pages.isEmpty()
					|| cost.isEmpty() || publisher.isEmpty()
					|| Integer.parseInt(pages) < 1
					|| Integer.parseInt(cost) < 0;
		} catch (NumberFormatException e) {
			flag = true;
			log.error("Comes Issue string field without number");
		}
		return flag;
	}

	public static Issue updateIssue(String idIssue, String name,
			String cathegory, String pages, String cost, String publisher) {
		if (issueValidation(name, cathegory, pages, cost, publisher)) {
			return null;
		}
		IssueDAO dao = new IssueDAO();
		Issue issue = new Issue();
		issue.setId(Integer.parseInt(idIssue));
		issue.setName(name);
		issue.setIdCathegory(Integer.parseInt(cathegory));
		issue.setIdPublisher(Integer.parseInt(publisher));
		issue.setMonthCost(Integer.parseInt(cost));
		issue.setPages(Integer.parseInt(pages));
		return dao.update(issue);

	}
}
