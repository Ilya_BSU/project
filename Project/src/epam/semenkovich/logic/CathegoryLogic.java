package epam.semenkovich.logic;

import java.util.List;

import epam.semenkovich.dao.CathegoryDAO;
import epam.semenkovich.entity.Cathegory;

public class CathegoryLogic {
	/**
	 * Gets all categories from SQL table to list with Cathegory
	 * parameterization
	 * 
	 * @return List with all categories from SQL
	 */
	public static List<Cathegory> recieveCathegorys() {
		CathegoryDAO dao = new CathegoryDAO();
		return dao.findAll();
	}

	/**
	 * Get category from SQL by its id
	 * 
	 * @param id
	 *            Cathegory's id in String form
	 * @return Catheory object which comes from database. Return null if there
	 *         is wrong param
	 */
	public static Cathegory getCathegoryById(String id) {
		if (id.isEmpty()) {
			return null;
		}
		CathegoryDAO dao = new CathegoryDAO();
		return dao.findEntityById(id);
	}

}
