package epam.semenkovich.logic;

import epam.semenkovich.dao.UserDAO;
import epam.semenkovich.entity.User;

public class LoginLogic {
	private static final int MIN_LOGIN_LENGHT = 4;
	private static final int MAX_LOGIN_LENGHT = 16;
	private static final int MIN_PASS_LENGHT = 4;

	/**
	 * Finds User in database by login and password
	 * 
	 * @param enterLogin
	 *            login you wanted to check in database
	 * @param enterPass
	 *            password you wanted to check in database
	 * @return User object, specified with given login and pass. Return
	 *         <b>null</b> if there are empty params or there is not such User
	 *         in database.
	 */
	public static User checkLogin(String enterLogin, String enterPass) {
		if (enterLogin.isEmpty() || enterPass.isEmpty()) {
			return null;
		}
		UserDAO dao = new UserDAO();
		User user = dao.findUserByLoginAndPass(enterLogin, enterPass);
		return user;
	}

	/**
	 * Creates new User and add information to database
	 * 
	 * @param enterLogin
	 *            User's login. <b>Should contains from 4 to 16 symbols </b>
	 * @param enterPass
	 *            User's password. <b>Should contains from 4 to 16 symbols </b>
	 * @return User object, specified with given login and password. Returns
	 *         null if it fails during SQL request or there are wrong params.
	 */
	public static User createUser(String enterLogin, String enterPass) {
		if (loginValidation(enterLogin, enterPass)) {
			return null;
		}
		UserDAO dao = new UserDAO();
		User user = new User();
		user.setLogin(enterLogin);
		user.setPassword(enterPass);
		user.setRole("USER");
		return dao.create(user) ? user : null;
	}

	private static boolean loginValidation(String login, String pass) {
		boolean flag = false;
		flag = login.length() < MIN_LOGIN_LENGHT
				|| login.length() > MAX_LOGIN_LENGHT
				|| pass.length() < MIN_PASS_LENGHT
				|| pass.length() > MAX_LOGIN_LENGHT;
		return flag;
	}
}
