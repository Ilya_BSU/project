package epam.semenkovich.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class DeleteIssueCommand implements ActionCommand {
	private static final String PARAM_ISSUE_LIST = "issues";
	private static final String PARAM_DELETE_ID = "delID";
	private static final String PARAM_ERROR_MSG = "errorMessage";
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String delID = request.getParameter(PARAM_DELETE_ID);
		if (IssueLogic.deleteIssueById(delID)) {
			page = ConfigurationManager.getProperty("path.page.main");
			List<Issue> issues = IssueLogic.recieveIssues();
			request.setAttribute(PARAM_ISSUE_LIST, issues);
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_Issue_Del"));
			page = ConfigurationManager.getProperty("path.page.error");
		}
		return page;
	}
}
