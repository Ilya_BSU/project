package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.logic.CathegoryLogic;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.logic.PublisherLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class EditIssueRedirectCommand implements ActionCommand {
	private static final String PARAM_EDIT_ID = "editID";
	private static final String PARAM_CURRENT_ISSUE = "currentIssue";
	private static final String PARAM_SELECTED_CATHEGORY = "selectedCathegory";
	private static final String PARAM_SELECTED_PUBLISHER = "selectedPublisher";
	private static final String PARAM_PUBLISHER_LIST = "publisherList";
	private static final String PARAM_CATHEGORY_LIST = "cathegoryList";
	private static final String PARAM_ERROR_MSG = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String editID = request.getParameter(PARAM_EDIT_ID);
		Issue current = IssueLogic.getIssueById(editID);
		if (current != null) {
			page = ConfigurationManager.getProperty("path.page.editIssue");
			request.setAttribute(PARAM_CURRENT_ISSUE, current);
			request.setAttribute(PARAM_SELECTED_CATHEGORY, CathegoryLogic
					.getCathegoryById(String.valueOf(current.getIdCathegory())));
			request.setAttribute(PARAM_SELECTED_PUBLISHER, PublisherLogic
					.getPublisherById(String.valueOf(current.getIdPublisher())));
			request.setAttribute(PARAM_PUBLISHER_LIST,
					PublisherLogic.recievePublishers());
			request.setAttribute(PARAM_CATHEGORY_LIST,
					CathegoryLogic.recieveCathegorys());
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_Issue_get"));
			page = ConfigurationManager.getProperty("path.page.error");
		}
		return page;
	}
}