package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.logic.OrderLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class AddOrderCommand implements ActionCommand {
	private static final String PARAM_CHECK_BOX = "orderCheckBox";
	private static final String PARAM_USER = "user";
	private static final String PARAM_ORDER_LIST = "orderList";
	private static final String PARAM_ERROR_MSG = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String[] issueID = null;
		issueID = request.getParameterValues(PARAM_CHECK_BOX);
		String user = (String) request.getSession().getAttribute(PARAM_USER);
		boolean flag = false;
		if (issueID != null) {
			for (int i = 0; i < issueID.length; i++) {
				flag = OrderLogic.addOrder(user, issueID[i], (String
						.valueOf(IssueLogic.getIssueById(issueID[i])
								.getMonthCost())));
			}
		}
		if (flag) {
			page = ConfigurationManager.getProperty("path.page.orderList");
			request.setAttribute(PARAM_ORDER_LIST,
					OrderLogic.getOrdersByUser(user));
		} else {
			request.setAttribute(PARAM_ERROR_MSG, new MessageManager()
					.getProperty("message.empty_Selecthion"));
			page = ConfigurationManager.getProperty("path.page.error");
		}
		return page;
	}

}
