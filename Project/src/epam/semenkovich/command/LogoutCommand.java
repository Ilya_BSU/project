package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.resource.ConfigurationManager;

public class LogoutCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager.getProperty("path.page.index");
		request.getSession().invalidate();// sessionDestroy
		return page;
	}

}
