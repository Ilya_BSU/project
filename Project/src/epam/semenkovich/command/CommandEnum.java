package epam.semenkovich.command;

public enum CommandEnum {
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	REGISTRATION {
		{
			this.command = new RegistrationCommand();
		}
	},
	LOGIN_REDIRECT {
		{
			this.command = new LoginRedirectCommand();
		}
	},
	ORDER_LIST {
		{
			this.command = new OrderListRedirectCommand();
		}
	},
	ADD_ORDER {
		{
			this.command = new AddOrderCommand();
		}
	},
	DELETE_ORDER {
		{
			this.command = new DeleteOrderCommand();
		}
	},
	PAY_ORDER {
		{
			this.command = new PayOrderCommand();
		}
	},
	ADD_ISSUE_REDIRECT {
		{
			this.command = new AddIssueRedirectCommand();
		}
	},
	ADD_ISSUE {
		{
			this.command = new AddIssueCommand();
		}
	},
	EDIT_ISSUE {
		{
			this.command = new EditIssueRedirectCommand();
		}
	},
	UPDATE_ISSUE {
		{
			this.command = new UpdateIssueCommand();
		}
	},
	DELETE_ISSUE {
		{
			this.command = new DeleteIssueCommand();
		}
	},
	ADD_PUBLISHER_REDIRECT {
		{
			this.command = new AddPublisherRedirectCommand();
		}
	},
	ADD_PUBLISHER {
		{
			this.command = new AddPublisherCommand();
		}
	},

	LOCALE {
		{
			this.command = new LocaleCommand();
		}
	},
	BACK {
		{
			this.command = new BackCommand();
		}
	},
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	};
	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}
