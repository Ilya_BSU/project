package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.logic.CathegoryLogic;
import epam.semenkovich.logic.PublisherLogic;
import epam.semenkovich.resource.ConfigurationManager;

public class AddIssueRedirectCommand implements ActionCommand {
	private static final String PARAM_PUBLISHER_LIST = "publisherList";
	private static final String PARAM_CATHEGORY_LIST = "cathegoryList";
		@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		request.setAttribute(PARAM_PUBLISHER_LIST,
				PublisherLogic.recievePublishers());
		request.setAttribute(PARAM_CATHEGORY_LIST,
				CathegoryLogic.recieveCathegorys());
		page = ConfigurationManager.getProperty("path.page.addIssue");
		return page;
	}
}