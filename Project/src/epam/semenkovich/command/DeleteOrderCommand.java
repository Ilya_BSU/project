package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.logic.OrderLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class DeleteOrderCommand implements ActionCommand {
	private static final String PARAM_ORDER_LIST = "orderList";
	private static final String PARAM_DELETE_ID = "delID";
	private static final String PARAM_ERROR_MSG = "errorMessage";
	private static final String PARAM_USER = "user";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String delID = request.getParameter(PARAM_DELETE_ID);
		if (OrderLogic.deleteOrderById(delID)) {
			page = ConfigurationManager.getProperty("path.page.orderList");
			request.setAttribute(PARAM_ORDER_LIST, OrderLogic
					.getOrdersByUser((String) request.getSession()
							.getAttribute(PARAM_USER)));
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_Order_Del"));
			page = ConfigurationManager.getProperty("path.page.error");
		}
		return page;
	}
}
