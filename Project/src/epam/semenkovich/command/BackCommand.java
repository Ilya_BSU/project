package epam.semenkovich.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.resource.ConfigurationManager;

public class BackCommand implements ActionCommand {
	private static final String PARAM_ISSUE_LIST = "issues";

	@Override
	public String execute(HttpServletRequest request) {
		List<Issue> issues = IssueLogic.recieveIssues();
		request.setAttribute(PARAM_ISSUE_LIST, issues);
		String page=ConfigurationManager.getProperty("path.page.main");
		return page;
	}
}
 