package epam.semenkovich.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;






import epam.semenkovich.entity.Issue;
import epam.semenkovich.entity.User;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.logic.LoginLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class LoginCommand implements ActionCommand {
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_ROLE = "role";
	private static final String PARAM_USER = "user";
	private static final String PARAM_ISSUE_LIST = "issues";
	private static final String PARAM_ERROR_MSG = "errorLoginPassMessage";
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String login = request.getParameter(PARAM_LOGIN);
		String pass = request.getParameter(PARAM_PASSWORD);
		User currentUser = LoginLogic.checkLogin(login, pass);
		if (currentUser != null) {
			request.getSession().setAttribute(PARAM_USER, login);
			request.getSession().setAttribute(PARAM_ROLE, currentUser.getRole());
			page = ConfigurationManager.getProperty("path.page.main");
			List<Issue> issues = IssueLogic.recieveIssues();
			request.setAttribute(PARAM_ISSUE_LIST, issues);
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.login_error"));
			page = ConfigurationManager.getProperty("path.page.login");
		}
		return page;
	}
}
