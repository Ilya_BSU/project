package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.logic.OrderLogic;
import epam.semenkovich.resource.ConfigurationManager;

public class OrderListRedirectCommand implements ActionCommand {
	private static final String PARAM_ORDER_LIST = "orderList";
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		page = ConfigurationManager.getProperty("path.page.orderList");
		request.setAttribute(PARAM_ORDER_LIST, OrderLogic.getOrdersByUser((String) request
				.getSession().getAttribute("user")));
		return page;
	}
}