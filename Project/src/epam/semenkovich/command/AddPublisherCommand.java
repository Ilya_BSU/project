package epam.semenkovich.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.entity.Publisher;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.logic.PublisherLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class AddPublisherCommand implements ActionCommand {
	private static final String PARAM_NAME = "name";
	private static final String PARAM_ADDRESS = "address";
	private static final String PARAM_ISSUE_LIST = "issues";
	private static final String PARAM_ERROR_MSG = "errorMessage";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String name = request.getParameter(PARAM_NAME);
		String address = request.getParameter(PARAM_ADDRESS);
		Publisher currentPublisher = PublisherLogic.addPublisher(name, address);
		if (currentPublisher != null) {
			page = ConfigurationManager.getProperty("path.page.main");
			List<Issue> issues = IssueLogic.recieveIssues();
			request.setAttribute(PARAM_ISSUE_LIST, issues);
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_publisher"));
			page = ConfigurationManager.getProperty("path.page.addPublisher");
		}
		return page;
	}
}
