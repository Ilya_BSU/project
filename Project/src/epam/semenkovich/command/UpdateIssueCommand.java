package epam.semenkovich.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.logic.CathegoryLogic;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.logic.PublisherLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class UpdateIssueCommand implements ActionCommand {
	private static final String PARAM_ISSUE_ID = "currentID";
	private static final String PARAM_NAME = "name";
	private static final String PARAM_CATHEGORY = "cathegory";
	private static final String PARAM_PAGES = "pages";
	private static final String PARAM_COST = "cost";
	private static final String PARAM_PUBLISHER = "publisher";
	private static final String PARAM_ISSUE_LIST = "issues";
	private static final String PARAM_PUBLISHER_LIST = "publisherList";
	private static final String PARAM_CATHEGORY_LIST = "cathegoryList";
	private static final String PARAM_ERROR_MSG = "errorMessage";
	private static final String PARAM_CURRENT_ISSUE = "currentIssue";
	private static final String PARAM_SELECTED_CATHEGORY = "selectedCathegory";
	private static final String PARAM_SELECTED_PUBLISHER = "selectedPublisher";

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String name = request.getParameter(PARAM_NAME);
		String cathegory = request.getParameter(PARAM_CATHEGORY);
		String pages = request.getParameter(PARAM_PAGES);
		String cost = request.getParameter(PARAM_COST);
		String publisher = request.getParameter(PARAM_PUBLISHER);
		String currentID = request.getParameter(PARAM_ISSUE_ID);
		Issue updatedIssue = IssueLogic.updateIssue(currentID, name, cathegory,
				pages, cost, publisher);
		if (updatedIssue != null) {
			page = ConfigurationManager.getProperty("path.page.main");
			List<Issue> issues = IssueLogic.recieveIssues();
			request.setAttribute(PARAM_ISSUE_LIST, issues);
		} else {
			Issue current = IssueLogic.getIssueById(currentID);
			request.setAttribute(PARAM_CURRENT_ISSUE, current);
			request.setAttribute(PARAM_SELECTED_CATHEGORY, CathegoryLogic
					.getCathegoryById(String.valueOf(current.getIdCathegory())));
			request.setAttribute(PARAM_SELECTED_PUBLISHER, PublisherLogic
					.getPublisherById(String.valueOf(current.getIdPublisher())));
			request.setAttribute(PARAM_PUBLISHER_LIST,
					PublisherLogic.recievePublishers());
			request.setAttribute(PARAM_CATHEGORY_LIST,
					CathegoryLogic.recieveCathegorys());
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_Issue_Field"));
			page = ConfigurationManager.getProperty("path.page.editIssue");
		}
		return page;
	}
}
