package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class LoginRedirectCommand implements ActionCommand {
	private static final String PARAM_LOGIN_RULE = "LoginRule";
	private static final String PARAM_PASSWORD_RULE = "PassRule";

	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigurationManager
				.getProperty("path.page.registration");
		MessageManager messageManager = new MessageManager();
		request.setAttribute(PARAM_LOGIN_RULE,
				messageManager.getProperty("message.login_rule"));
		request.setAttribute(PARAM_PASSWORD_RULE,
				messageManager.getProperty("message.pass_rule"));
		return page;
	}
}