package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.logic.OrderLogic;
import epam.semenkovich.resource.ConfigurationManager;
import epam.semenkovich.resource.MessageManager;

public class PayOrderCommand implements ActionCommand {
	private static final String PARAM_ORDER_LIST = "orderList";
	private static final String PARAM_ERROR_MSG = "errorMessage";
	private static final String PARAM_USER = "user";
	private static final String PARAM_PAY_ID = "payID";
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String payID = request.getParameter(PARAM_PAY_ID);
		if (OrderLogic.PayOrder(payID)) {
			page = ConfigurationManager.getProperty("path.page.orderList");
			request.setAttribute(PARAM_ORDER_LIST, OrderLogic.getOrdersByUser((String) request
					.getSession().getAttribute(PARAM_USER)));
		} else {
			request.setAttribute(PARAM_ERROR_MSG,
					new MessageManager().getProperty("message.wrong_order_pay"));
			page = ConfigurationManager.getProperty("path.page.error");
		}
		return page;
	}
}
