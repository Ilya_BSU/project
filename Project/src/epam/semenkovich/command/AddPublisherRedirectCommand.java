package epam.semenkovich.command;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.resource.ConfigurationManager;

public class AddPublisherRedirectCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		page = ConfigurationManager.getProperty("path.page.addPublisher");
		return page;
	}
}