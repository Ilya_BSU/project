package epam.semenkovich.command;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.entity.Issue;
import epam.semenkovich.logic.IssueLogic;
import epam.semenkovich.resource.ConfigurationManager;

public class LocaleCommand implements ActionCommand {
	private static final String PARAM_LANGUAGE = "language";
	private static final String PARAM_ISSUE_LIST = "issues";
	private static final String PARAM_RU = "ru";
	private static final String PARAM_EN = "en";

	@Override
	public String execute(HttpServletRequest request) {
		String language = (String) request.getSession().getAttribute(
				PARAM_LANGUAGE);
		String newLanguage = PARAM_EN.equals(language) ? PARAM_RU : PARAM_EN;
		Locale.setDefault(new Locale(newLanguage));
		request.getSession(true).setAttribute(PARAM_LANGUAGE, newLanguage);
		List<Issue> issues = IssueLogic.recieveIssues();
		request.setAttribute(PARAM_ISSUE_LIST, issues);
		String page = ConfigurationManager.getProperty("path.page.main");
		return page;
	}
}
