package epam.semenkovich.test;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import epam.semenkovich.exception.ResourceException;
import epam.semenkovich.pool.ConnectionPool;
import java.util.LinkedList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConnectionPoolTest {
	private ConnectionPool connectionPool;

	@Before
	public void testGetInstance() {
		connectionPool = ConnectionPool.getInstance();
		assertNotNull("Pool wasn't created", connectionPool);
	}

	@After
	public void testCloseConnection() {
		connectionPool.closePool();
	}

	@Test
	public void testConnectionNumber() throws ResourceException {

		LinkedList<Connection> connections = new LinkedList<>();
		for (int i = 0; i < ConnectionPool.POOL_SIZE; i++) {
			Connection connection = connectionPool.getResource();
			assertNotNull("Connection was not received from pool", connection);
			connections.add(connection);
		}
		for (Connection connection : connections) {
			connectionPool.returnResource(connection);
		}

	}
}
