package epam.semenkovich.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import java.io.IOException;

@SuppressWarnings("serial")
public class UserInfoTag extends TagSupport {
	private String name;
	private String role;

	public void setRole(String role) {
		this.role = role;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			String language = (String) pageContext.getSession().getAttribute(
					"language");
			String to = null;
			switch (language) {
			case "en":
				if ("admin".equalsIgnoreCase(role)) {
					to = "Role: " + "<font color=\"DarkRed\"> <b>" + role
							+ "</b></font>" + "; Hello <b>" + name + "</b>.";
				} else if (role.isEmpty()) {
					to = "Hello, <b>guest</b>.";
				} else {
					to = "Role: " + role + "; Hello <b>" + name + "</b>.";
				}
				pageContext.getOut().write("<br>"+to + "</br>");
				break;
			case "ru":
				if ("admin".equalsIgnoreCase(role)) {
					to = "����: " + "<font color=\"DarkRed\"> <b>" + role
							+ "</b></font>" + "; ����������� <b>" + name
							+ "</b>.";
				} else if (role.isEmpty()) {
					to = "�����������, <b>�����.</b>";
				} else {
					to = "����: " + role + "; ����������� <b>" + name + "</b>.";
				}

				pageContext.getOut().write("<br>"+to + "</br>");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
