package epam.semenkovich.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class PayTag extends TagSupport {
	private int isPaid;

	public int getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(int isPaid) {
		this.isPaid = isPaid;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			String language = (String) pageContext.getSession().getAttribute(
					"language");
			String to = null;
			switch (language) {
			case "en":
				if (isPaid == 1) {
					to = "Paid";
					//	to = "<fmt:message key=\"tag.paid\" />";
					pageContext.getOut().write(
							"<font color=\"ForestGreen\"> <b>" + to + "</b></font>");
				} else {
					to = "Unpaid";
					pageContext.getOut().write(
							"<font color=\"DarkRed\"> <b>" + to + "</b></font>");
				}

				break;
			case "ru":
				if (isPaid == 1) {
					to = "��������";
					pageContext.getOut().write(
							"<font color=\"ForestGreen\"><b>" + to + "</b></font>");
				} else {
					to = "����������";
					pageContext.getOut().write(
							"<font color=\"DarkRed\"><b>" + to + "</b></font>");
				}
				break;
			}

		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
