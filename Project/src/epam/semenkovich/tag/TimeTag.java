package epam.semenkovich.tag;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class TimeTag extends TagSupport {

	@Override
	public int doStartTag() throws JspException {
		try {
			GregorianCalendar gc = new GregorianCalendar();
			String time = "<br>Time : <b> " + gc.getTime() + " </b><br>";
			pageContext.getOut().write(time);
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
