package epam.semenkovich.resource;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {
	private ResourceBundle resourceBundle;

	public MessageManager() {
		resourceBundle = ResourceBundle.getBundle("resources/messages",
				Locale.getDefault());
	}

	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
