package epam.semenkovich.listener;

import java.util.Locale;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {
	private static final String PARAM_EN = "en";

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		HttpSession session = event.getSession();
		session.setAttribute("language", PARAM_EN);
		Locale.setDefault(new Locale(PARAM_EN));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {

	}
}
