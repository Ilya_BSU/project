package epam.semenkovich.entity;

@SuppressWarnings("serial")
public class Publisher extends Entity {

	private int idPublisher;
	private String name;
	private String address;

	public Publisher() {
		super();
	}

	public Publisher(int idPublisher, String name, String address) {
		this.idPublisher = idPublisher;
		this.name = name;
		this.address = address;
	}

	public int getIdPublisher() {
		return idPublisher;
	}

	public void setIdPublisher(int idPublisher) {
		this.idPublisher = idPublisher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
