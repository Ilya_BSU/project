package epam.semenkovich.entity;

import java.util.Map;

@SuppressWarnings("serial")
public class Issue extends Entity {

	private int id;
	private String name;
	private int idCathegory;
	private int pages;
	private int monthCost;
	private int idPublisher;
	/**
	 * @value Contains idCathegoty as a key and Cathegory name as a value
	 */
	private Map<Integer, String> id_name_cathegory = null;
	/**
	 * @value Contains idPublisher as a key and Publisher name as a value
	 */
	private Map<Integer, String> id_name_publisher = null;

	public Issue() {
	}

	public Issue(int id, String name, int idCathegory, int pages,
			int monthCost, int idPublisher) {
		this.id = id;
		this.name = name;
		this.idCathegory = idCathegory;
		this.pages = pages;
		this.monthCost = monthCost;
		this.idPublisher = idPublisher;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIdCathegory() {
		return idCathegory;
	}

	public void setIdCathegory(int idCathegory) {
		this.idCathegory = idCathegory;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getMonthCost() {
		return monthCost;
	}

	public void setMonthCost(int monthCost) {
		this.monthCost = monthCost;
	}

	public int getIdPublisher() {
		return idPublisher;
	}

	public void setIdPublisher(int idPublisher) {
		this.idPublisher = idPublisher;
	}

	public Map<Integer, String> getId_name_cathegory() {
		return id_name_cathegory;
	}

	public void setId_name_cathegory(Map<Integer, String> id_name_cathegory) {
		this.id_name_cathegory = id_name_cathegory;
	}

	public Map<Integer, String> getId_name_publisher() {
		return id_name_publisher;
	}

	public void setId_name_publisher(Map<Integer, String> id_name_publisher) {
		this.id_name_publisher = id_name_publisher;
	}

}
