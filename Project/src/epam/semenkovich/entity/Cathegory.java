package epam.semenkovich.entity;
@SuppressWarnings("serial")
public class Cathegory extends Entity {
	
	private int idCathegory;
	private String name;

	public Cathegory() {
		
	}

	public Cathegory(int idCathegory, String name) {
		this.idCathegory = idCathegory;
		this.name = name;
	}

	public int getIdCathegory() {
		return idCathegory;
	}

	public void setIdCathegory(int idCathegory) {
		this.idCathegory = idCathegory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
