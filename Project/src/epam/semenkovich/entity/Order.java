package epam.semenkovich.entity;

import java.util.Map;

@SuppressWarnings("serial")
public class Order extends Entity {

	private int idOrder;
	private String user;
	private int idIssue;
	private int totalCost;
	private int isPaid;
	/**
	 * @value Contains idIssue as a key and Issue name as a value
	 */
	private Map<Integer, String> id_name_issue = null;

	public Order() {
	}

	public Order(String user, int idIssue, int totalCost, int isPaid) {
		this.user = user;
		this.idIssue = idIssue;
		this.totalCost = totalCost;
		this.isPaid = isPaid;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getIdIssue() {
		return idIssue;
	}

	public void setIdIssue(int idIssue) {
		this.idIssue = idIssue;
	}

	public int getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}

	public int getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(int isPaid) {
		this.isPaid = isPaid;
	}

	public Map<Integer, String> getId_name_issue() {
		return id_name_issue;
	}

	public void setId_name_issue(Map<Integer, String> id_name_issue) {
		this.id_name_issue = id_name_issue;
	}

}
