package epam.semenkovich.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Hash {
	private static Logger log = Logger.getLogger(Hash.class);
	private static MessageDigest digester;

	public static String toHash(String pass) {
		StringBuffer hexString = null;
		if (pass.isEmpty()) {
			log.error("Pass is empty. Hash wasnt generated.");
		}
		try {
			digester = MessageDigest.getInstance("MD5");
			digester.update(pass.getBytes());
			byte[] hash = digester.digest();
			hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
				if ((0xff & hash[i]) < 0x10) {
					hexString.append("0"
							+ Integer.toHexString((0xFF & hash[i])));
				} else {
					hexString.append(Integer.toHexString(0xFF & hash[i]));
				}
			}
		} catch (NoSuchAlgorithmException ex) {
			log.error("Error while generating hash." + ex.getMessage());
		}
		return hexString.toString();
	}

}
