package epam.semenkovich.factory;

import javax.servlet.http.HttpServletRequest;

import epam.semenkovich.command.ActionCommand;
import epam.semenkovich.command.CommandEnum;
import epam.semenkovich.command.EmptyCommand;
import epam.semenkovich.resource.MessageManager;

public class ActionFactory {
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = null;
		String action = request.getParameter("command");
		if (action == null || action.isEmpty()) {
			return new EmptyCommand();
		}
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute("errorMessage",
					action + new MessageManager().getProperty("message.wrongaction"));
		}
		return current;
	}
}
