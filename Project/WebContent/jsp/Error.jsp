<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="title.errorpage" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<c:if test="${not empty exception}">
		<%=exception.getMessage()%>
	</c:if>
	${errorMessage}
	<br/>
	<button onclick='history.back();'>
		<fmt:message key="text.back" />
	</button>

	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>
