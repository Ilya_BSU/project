<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/common.css" />
<title><fmt:message key="title.headerPage" /></title>
</head>
<body>
	<hr />
	<img src="images/logo.JPG" alt="logo" style="vertical-align: middle;" />
	<font color="blue" size="24" face="comic sans ms,century gothic">
		<fmt:message key="title.header" />
	</font>
	
	<ctg:userInfo name="${user}" role="${role}" />
	<c:if test="${sessionScope.role!=null}">
		<a href="controller?command=logout" style="text-align: right;"><fmt:message
				key="header.logout" /></a>
		<ctg:Time />		
		<form name="LanguageForm" method="POST" action="controller">
			<div style="float: right">
				<input type="hidden" name="command" value="LOCALE" /> <input
					type="submit" value="<fmt:message key="text.b_language" />" />
			</div>
		</form>
		<br />
	</c:if>
</body>
</html>