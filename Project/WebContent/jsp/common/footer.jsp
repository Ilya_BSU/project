<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/common.css" />
<title><fmt:message key="title.footer" /></title>
</head>
<body>
	<hr />
	<fmt:message key="footer.copyright" />
</body>
</html>
