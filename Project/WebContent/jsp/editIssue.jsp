<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="title.addIssue" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="update_Issue" /> <input
			type="hidden" name="currentID" value="${currentIssue.id}" />
		<fmt:message key="text.issue.name" />
		<br /> <input type="text" name="name" value="${currentIssue.name}"
			required="required" /> <br />
		<fmt:message key="text.issue.cathegory" />
		<br /> <select name="cathegory">
			<option value="${selectedCathegory.idCathegory}" selected><c:out
							value="${selectedCathegory.name}"/></option>
			<c:forEach var="cathegory" items="${cathegoryList}">
				<c:if
					test="${cathegory.idCathegory != selectedCathegory.idCathegory}">
					<option value="${cathegory.idCathegory} "><c:out
							value="${cathegory.name}"/>
					</option>
				</c:if>
			</c:forEach>
		</select> <br />
		<fmt:message key="text.issue.pages" />
		<br /> <input type="text" name="pages" value="${currentIssue.pages}"
			required="required" /> <br />
		<fmt:message key="text.issue.cost" />
		<br /> <input type="text" name="cost"
			value="${currentIssue.monthCost}" required="required" /> <br />
		<fmt:message key="text.issue.publisher" />
		<br /> <select name="publisher">
			<option value="${selectedPublisher.idPublisher}" selected><c:out
					value="${selectedPublisher.name}" /></option>
			<c:forEach var="publisher" items="${publisherList}">
				<c:if
					test="${publisher.idPublisher != selectedPublisher.idPublisher}">
					<option value="${publisher.idPublisher}"><c:out
							value="${publisher.name}" />
					</option>
				</c:if>
			</c:forEach>
		</select> <br /> ${errorMessage} <br /> <input type="submit"
			value="<fmt:message key="text.b_update_issue"/>" />
	</form>

	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="back" /> <input
			type="submit" value="<fmt:message key="text.back"/>" />
	</form>

	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>