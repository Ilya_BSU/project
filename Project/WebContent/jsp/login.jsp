<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="text.b_login" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<div align="center">
		<h2>
			<fmt:message key="text.login_button" />
		</h2>
		<form name="loginForm" method="POST" action="controller">
			<input type="hidden" name="command" value="login" />
			<fmt:message key="text.login" />
			<br /> <input type="text" name="login" value="" pattern=".{4,}" maxlength="16" required /> <br />
			<fmt:message key="text.password" />
			<br /> <input type="password" name="password" pattern=".{4,}" maxlength="16"	
				value="" required/><br /> ${errorLoginPassMessage} <br /> <input
				type="submit" value="<fmt:message key="text.b_login" />" />
		</form>
		<br />
		<form name="registrationForm" method="POST" action="controller">
			<input type="hidden" name="command" value="LOGIN_REDIRECT" /> <input
				type="submit" value="<fmt:message key="text.b_go_registration" />" />
		</form>
	</div>
	<hr />
	
	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>