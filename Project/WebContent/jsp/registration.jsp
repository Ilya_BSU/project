<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="text.b_registration" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<div align="center">
		<h2>
			<fmt:message key="text.registration_button" />
		</h2>
		<form name="RegForm" method="POST" action="controller">
			<input type="hidden" name="command" value="REGISTRATION" />
			<fmt:message key="text.login" />
			<br /> <input type="text" name="login" maxlength="16" value=""
				pattern=".{4,}" title="${LoginRule}" required /> <br />
			<fmt:message key="text.password" />
			<br /> <input type="password" name="password" value=""
				pattern=".{4,}" title="${PassRule}" maxlength="16" required /> <br />
			<input type="submit"
				value="<fmt:message key="text.b_registration" />" /> <br />
			${errorRegistrationMessage}
		</form>
		<br />
		<button onclick='history.back();'>
			<fmt:message key="text.back" />
		</button>
	</div>
	<hr />

	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>
