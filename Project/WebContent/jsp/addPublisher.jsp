<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="title.addPublisher" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<form name="publisherForm" method="POST" action="controller">
		<input type="hidden" name="command" value="add_Publisher" />
		<fmt:message key="text.publisher.name" />
		<br /> <input type="text" name="name" value="" required /> <br />
		<fmt:message key="text.publisher.address" />
		<br /> <input type="text" name="address" value="" /> <br />
		${errorMessage} <br /> <input type="submit"
			value="<fmt:message key="text.b_add_publisher"/>" />
	</form>
	<hr />
	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="back" /> <input
			type="submit" value="<fmt:message key="text.back"/>" />
	</form>
	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>