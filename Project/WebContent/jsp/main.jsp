<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<link rel="stylesheet" type="text/css" href="css/table.css" />
<title><fmt:message key="title.main" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<%-- Кодировка запроса: ${ pageContext.request.characterEncoding } --%>
	<table class="layout">
		<tr class="layout">
			<c:if test="${sessionScope.role=='ADMIN'}">
				<td>
					<form name="issueAddForm" method="POST" action="controller">
						<input type="hidden" name="command" value="add_Issue_Redirect" /> <input
							type="submit" value="<fmt:message key="text.b_add_issue" />" />
					</form>
				</td>
				<td><form name="publisherAddForm" method="POST"
						action="controller">
						<input type="hidden" name="command" value="add_Publisher_Redirect" />
						<input type="submit"
							value="<fmt:message key="text.b_add_publisher" />" />
					</form></td>
			</c:if>
			<td>
				<form name="publisherAddForm" method="POST" action="controller">
					<input type="hidden" name="command" value="order_List" /> <input
						type="submit" value="<fmt:message key="text.b_order_List" />" />
				</form>
			</td>
		</tr>
	</table>
	<form>
		<table border="1" width="100%">
			<caption>
				<fmt:message key="text.catalogue" />
			</caption>
			<tr>
				<th><fmt:message key="issue.id" /></th>
				<th><fmt:message key="issue.name" /></th>
				<th><fmt:message key="issue.cathegory" /></th>
				<th><fmt:message key="issue.pages" /></th>
				<th><fmt:message key="issue.cost" /></th>
				<th><fmt:message key="issue.publisher" /></th>
				<c:if test="${sessionScope.role=='ADMIN'}">
				<th></th>
				<th></th>
				</c:if>
				<th><fmt:message key="issue.order" /></th>
			</tr>
			<c:forEach var="elem" items="${issues}">
				<tr align="center">
					<td align="center"><c:out value="${ elem.id }" /></td>
					<td align="center"><c:out value="${ elem.name }" /></td>
					<td align="center"><c:out value="${ elem.id_name_cathegory.get(elem.idCathegory) }" /></td>
					<td align="center"><c:out value="${ elem.pages }" /></td>
					<td align="center"><c:out value="${ elem.monthCost }" /></td>
					<td align="center"><c:out value="${ elem.id_name_publisher.get(elem.idPublisher) }" /></td>
					<c:if test="${sessionScope.role=='ADMIN'}">
						<td><a
							href="controller?command=DELETE_ISSUE&delID=${ elem.id }"> <fmt:message
									key="text.b_del_issue" />
						</a></td>
						<td><a
							href="controller?command=EDIT_ISSUE&editID=${ elem.id }"> <fmt:message
									key="text.b_edit_issue" />
						</a></td>
					</c:if>
					<td align="center"><input type="checkbox" name="orderCheckBox"
						value="${ elem.id }" /></td>
				</tr>
			</c:forEach>
		</table>
		<div style="float: right; padding: 10px">
			<input type="hidden" name="command" value="add_Order" /> <input
				type="submit" value="<fmt:message key="text.b_add_Order" />" />
		</div>
	</form>
	<div class="space"></div>
	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>

