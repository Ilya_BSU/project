<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld"%>
<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<link rel="stylesheet" type="text/css" href="css/table.css" />
<title><fmt:message key="title.orderList" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<form>
		<table border="1" width="100%">
			<tr>
				<th><fmt:message key="issue.name" /></th>
				<th><fmt:message key="issue.cost" /></th>
				<th><fmt:message key="order.status" /></th>
			</tr>
			<c:forEach var="elem" items="${orderList}">
				<tr align="center">
					<td align="center"><c:out
							value="${  elem.id_name_issue.get(elem.idIssue) }" /></td>
					<td align="center"><c:out value="${ elem.totalCost }" /></td>
					<td align="center"><ctg:Pay isPaid="${ elem.isPaid }" /></td>
					<td><a
						href="controller?command=DELETE_ORDER&delID=${ elem.idOrder }">
							<fmt:message key="text.b_del_issue" />
					</a></td>
					<td><a
						href="controller?command=PAY_ORDER&payID=${ elem.idOrder }"> <fmt:message
								key="text.b_pay" />
					</a></td>
				</tr>
			</c:forEach>
		</table>
	</form>
	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="back" /> <input
			type="submit" value="<fmt:message key="text.back"/>" />
	</form>

	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>