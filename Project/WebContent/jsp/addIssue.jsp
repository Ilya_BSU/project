<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setLocale value="${language}" scope="session" />
<fmt:setBundle basename="resources.text" />
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css" />
<title><fmt:message key="title.addIssue" /></title>
</head>
<body>
	<div class="header">
		<c:import url="common\header.jsp" />
	</div>
	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="add_Issue" />
		<fmt:message key="text.issue.name" />
		<br /> <input type="text" name="name" value="" required="required" />
		<br />
		<fmt:message key="text.issue.cathegory" />
		<br /> <select name="cathegory">
			<c:forEach var="cathegory" items="${cathegoryList}">
				<option value="${cathegory.idCathegory}"><c:out value="${cathegory.name}"/>
				</option>
				<br />
			</c:forEach>
		</select> <br />
		<fmt:message key="text.issue.pages" />
		<br /> <input type="text" name="pages" value=""
			required="required" placeholder="<fmt:message key="text.numeric"/>" />
		<br />
		<fmt:message key="text.issue.cost" />
		<br /> <input type="text" name="cost" value="" required="required"
			placeholder="<fmt:message key="text.numeric"/>" /> <br />
		<fmt:message key="text.issue.publisher" />
		<br /> <select name="publisher">
			<c:forEach var="publisher" items="${publisherList}">
				<option value="${publisher.idPublisher}"><c:out value="${publisher.name}"/>
				</option>
				<br />
			</c:forEach>
		</select> <br /> ${errorMessage} <br /> <input type="submit"
			value="<fmt:message key="text.b_add_issue"/>" />
	</form>
	<hr />
	<form name="issueForm" method="POST" action="controller">
		<input type="hidden" name="command" value="back" /> 
		<input type="submit" value="<fmt:message key="text.back"/>"/>
	</form>

	<div class="footer">
		<c:import url="common\footer.jsp" />
	</div>
</body>
</html>